import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.control.Separator
import javafx.scene.control.ToolBar
import javafx.scene.image.ImageView

class ToolbarView(val model:Model) : ToolBar(), IView {
    private var BUTTON_HEIGHT = 25

    init {
        val blockButton = Button("Block")
        val blockGraphic = ImageView("block.png")
        blockGraphic.isPreserveRatio = true
        blockGraphic.fitHeight = BUTTON_HEIGHT.toDouble()
        blockButton.graphic = blockGraphic
        blockButton.onMouseClicked = EventHandler { model.shape = Model.SHAPE.BLOCK }

        val beehiveButton = Button("Beehive")
        val beehiveGraphic = ImageView("beehive.png")
        beehiveGraphic.isPreserveRatio = true
        beehiveGraphic.fitHeight = BUTTON_HEIGHT.toDouble()
        beehiveButton.graphic = beehiveGraphic
        beehiveButton.onMouseClicked = EventHandler { model.shape = Model.SHAPE.BEEHIVE }

        val blinkerButton = Button("Blinker")
        val blinkerGraphic = ImageView("blinker.png")
        blinkerGraphic.isPreserveRatio = true
        blinkerGraphic.fitHeight = BUTTON_HEIGHT.toDouble()
        blinkerButton.graphic = blinkerGraphic
        blinkerButton.onMouseClicked = EventHandler { model.shape = Model.SHAPE.BLINKER }

        val toadButton = Button("Toad")
        val toadGraphic = ImageView("toad.png")
        toadGraphic.isPreserveRatio = true
        toadGraphic.fitHeight = BUTTON_HEIGHT.toDouble()
        toadButton.graphic = toadGraphic
        toadButton.onMouseClicked = EventHandler { model.shape = Model.SHAPE.TOAD }

        val gliderButton = Button("Glider")
        val gliderGraphic = ImageView("glider.png")
        gliderGraphic.isPreserveRatio = true
        gliderGraphic.fitHeight = BUTTON_HEIGHT.toDouble()
        gliderButton.graphic = gliderGraphic
        gliderButton.onMouseClicked = EventHandler { model.shape = Model.SHAPE.GLIDER }

        val clearButton = Button("Clear")
        val clearGraphic = ImageView("clear.png")
        clearGraphic.isPreserveRatio = true
        clearGraphic.fitHeight = BUTTON_HEIGHT.toDouble()
        clearButton.graphic = clearGraphic
        clearButton.onMouseClicked = EventHandler {
            model.shape = Model.SHAPE.NONE
            model.clearGrid()
        }

        this.items.addAll(
            blockButton, beehiveButton, Separator(), Separator(),
            blinkerButton, toadButton, gliderButton, Separator(), Separator(),
            clearButton
        )
    }

    override fun update() {
        // do nothing
    }
}
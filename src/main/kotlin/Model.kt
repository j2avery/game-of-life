import javafx.application.Platform
import java.util.*

class Model {
    var COLS = 80
    var ROWS = 50
    var grid = Array(COLS) { BooleanArray(ROWS) }

    enum class SHAPE {NONE, BLOCK, BEEHIVE, BLINKER, TOAD, GLIDER}
    var shape = SHAPE.BLOCK

    private val views = ArrayList<IView>()

    private var timer: Timer? = null
    private var TIMER_INTERVAL = 500

    var status = "Empty board"
    var frame = 0

    // view methods
    fun addView(view:IView) {
        views.add(view)
    }

    private fun notifyViews() {
        for (view in views) view.update()
    }

    // grid
    fun clearGrid() {
        for (col in 0 until COLS) {
            for (row in 0 until ROWS) {
                grid[col][row] = false
            }
        }
        status = "Cleared"
    }

    fun createShape(col: Int, row: Int) {
        when (shape) {
            SHAPE.BLOCK -> createBlock(col, row)
            SHAPE.BLINKER -> createBlinker(col, row)
            SHAPE.BEEHIVE -> createBeehive(col, row)
            SHAPE.GLIDER -> createGlider(col, row)
            SHAPE.TOAD -> createToad(col, row)
            SHAPE.NONE -> createBlock(col, row)
        }
    }

    // grid manipulation
    private fun createBlock(col: Int, row: Int) {
        if (outOfBounds(col, row, 2, 2)) return
        grid[col][row] = true // row 1
        grid[col + 1][row] = true
        grid[col][row + 1] = true // row 2
        grid[col + 1][row + 1] = true
        status = "Created block at $col, $row"
    }

    private fun createBeehive(col: Int, row: Int) {
        if (outOfBounds(col, row, 4, 3)) return
        grid[col][row] = false // row 1
        grid[col + 1][row] = true
        grid[col + 2][row] = true
        grid[col + 3][row] = false
        grid[col][row + 1] = true // row 2
        grid[col + 1][row + 1] = false
        grid[col + 2][row + 1] = false
        grid[col + 3][row + 1] = true
        grid[col][row + 2] = false // row 3
        grid[col + 1][row + 2] = true
        grid[col + 2][row + 2] = true
        grid[col + 3][row + 2] = false
        status = "Created beehive at $col, $row"
    }

    private fun createBlinker(col: Int, row: Int) {
        if (outOfBounds(col, row, 3, 3)) return
        grid[col][row] = false      // row 1
        grid[col + 1][row] = false
        grid[col + 2][row] = false
        grid[col][row + 1] = true   // row 2
        grid[col + 1][row + 1] = true
        grid[col + 2][row + 1] = true
        grid[col][row + 2] = false  // row 3
        grid[col + 1][row + 2] = false
        grid[col + 2][row + 2] = false
        status = "Created blinker at $col, $row"
    }

    private fun createToad(col: Int, row: Int) {
        if (outOfBounds(col, row, 3, 3)) return
        grid[col][row] = false      // row 1
        grid[col + 1][row] = true
        grid[col + 2][row] = true
        grid[col + 3][row] = true
        grid[col][row + 1] = true   // row 2
        grid[col + 1][row + 1] = true
        grid[col + 2][row + 1] = true
        grid[col + 3][row + 1] = false
        status = "Created toad at $col, $row"
    }

    private fun createGlider(col: Int, row: Int) {
        grid[col][row] = false      // row 1
        grid[col + 1][row] = false
        grid[col + 2][row] = true
        grid[col][row + 1] = true   // row 2
        grid[col + 1][row + 1] = false
        grid[col + 2][row + 1] = true
        grid[col][row + 2] = false  // row 3
        grid[col + 1][row + 2] = true
        grid[col + 2][row + 2] = true
        status = "Created glider at $col, $row"
    }

    private fun outOfBounds(x: Int, y: Int, cols: Int, rows: Int): Boolean {
        return x < 0 || x > COLS - 1 - cols || y < 0 || y > ROWS - 1 - rows
    }

    // timer code
    fun start() {
        timer = Timer()
        timer!!.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                Platform.runLater() {
                    updateGrid()
                    frame++
                    notifyViews()
                }
            }
        }, 0, 1000.also { TIMER_INTERVAL = it }.toLong())
    }

    fun stop() {
        if (timer != null) {
            timer!!.cancel()
        }
    }

    // grid updates
    // never called directly, only through the timer above
    private fun updateGrid() {
        val updated = Array(COLS) { BooleanArray(ROWS) }

        // do not update in place, but save results for now
        for (row in 0 until ROWS) {
            for (col in 0 until COLS) {
                val count = countNeighbours(col, row)
                val alive = grid[col][row]

                // Any dead cell with exactly three live neighbours becomes a live cell
                // Handle this case first!
                if (!alive && count == 3) {
                    updated[col][row] = true

                // Any live cell with fewer than two live neighbours dies
                } else if (alive && count < 2) {
                    updated[col][row] = false

                // Any live cell with two or three live neighbours lives
                } else if (alive && (count == 2 || count == 3)) {
                    updated[col][row] = true

                // Any live cell with more than three live neighbours dies
                } else if (alive && count > 3) {
                    updated[col][row] = false

                // default is false
                } else {
                    updated[col][row] = false
                }
            }
        }

        // update original once we've completed the pass
        for (row in 0 until ROWS) {
            for (col in 0 until COLS) {
                grid[col][row] = updated[col][row]
            }
        }
    }

    private fun countNeighbours(col: Int, row: Int): Int {
        var sum = 0
        sum += count(col - 1, row - 1) // top row
        sum += count(col, row - 1)
        sum += count(col + 1, row - 1)
        sum += count(col - 1, row) // same row
        sum += count(col + 1, row)
        sum += count(col - 1, row + 1) // bottom row
        sum += count(col, row + 1)
        sum += count(col + 1, row + 1)
        return sum
    }

    private fun count(col: Int, row: Int): Int {
        return if (col < 0 || col > COLS - 1 || row < 0 || row > ROWS - 1) {
            0
        } else {
            if (grid[col][row]) 1 else 0
        }
    }
}
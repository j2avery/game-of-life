import javafx.event.EventHandler
import javafx.scene.canvas.Canvas
import javafx.scene.input.MouseEvent
import javafx.scene.paint.Color

class GridView(val model:Model) : Canvas(model.COLS * 15.0, model.ROWS * 15.0), IView {
    init {
        onMouseClicked = EventHandler { event: MouseEvent ->
            val col = event.x.toInt() / 15.0
            val row = event.y.toInt() / 15.0
            model.createShape(col.toInt(), row.toInt())
        }
    }
    override fun update() {
        val gc = graphicsContext2D
        for (col in 0 until model.COLS) {
            for (row in 0 until model.ROWS) {
                // draw grid lines
                gc.stroke = Color.GRAY
                gc.strokeRect(col * 15.0, row * 15.0, 15.0, 15.0)

                // draw the cell
                if (model.grid[col][row]) {
                    gc.fill = Color.BLACK
                } else {
                    gc.fill = Color.WHITE
                }
                gc.fillRect(
                    col * 15.0 + 1,
                    row * 15.0 + 1,
                    15.0 - 2,
                    15.0 - 2
                )
            }
        }
    }
}
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.layout.FlowPane

class StatusView(val model:Model) : FlowPane(), IView {
    var status = Label("Cleared")
    var frame = Label("0")

    init {
        status.alignment = Pos.CENTER_LEFT
        status.padding = Insets(5.0)
        status.prefWidth = 1100.0

        frame.alignment = Pos.CENTER_RIGHT
        frame.padding = Insets(5.0)
        frame.prefWidth = 100.0

        children.addAll(status, frame)
    }

    override fun update() {
        status.text = model.status
        frame.text = model.frame.toString()
    }
}
import javafx.application.Application
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.layout.VBox
import javafx.stage.Stage

class Main : Application() {
    val WIDTH = 1200.0
    val HEIGHT = 825.0

    override fun start(stage: Stage) {
        // instantiate model and views
        val model = Model()
        val toolbar = ToolbarView(model)
        val canvas = GridView(model)
        val status = StatusView(model)

        // register views with the model so they receive updates
        model.addView(toolbar)
        model.addView(canvas)
        model.addView(status)

        // setup the scene
        stage.scene = Scene(VBox(toolbar, canvas, status), WIDTH, HEIGHT)
        stage.title = "Conway's Game of Life"
        stage.isResizable = false
        stage.onCloseRequest = EventHandler {
            model.stop()
        }
        stage.show()

        // start animation
        model.clearGrid()
        model.start()
    }
}